#include <Windows.h>
#include "d2dapp.h"
#include <math.h>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow);
LRESULT CALLBACK WndProc(HWND hInstance, UINT message, WPARAM wparam, LPARAM lparam);
d2dapp d2dApp;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow){


	WNDCLASS wc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hInstance = hInstance;
	wc.lpfnWndProc = WndProc;
	wc.lpszClassName = TEXT("InitD2D");

	wc.lpszMenuName = 0;
	wc.style = CS_HREDRAW | CS_VREDRAW;

	RegisterClass(&wc);

	FLOAT dpiX, dpiY;
	HDC screen = GetDC(0);
	dpiX = static_cast<FLOAT>(GetDeviceCaps(screen, LOGPIXELSX));
	dpiY = static_cast<FLOAT>(GetDeviceCaps(screen, LOGPIXELSY));

	int width=static_cast<UINT>(ceil(800.f * dpiX / 96.f));
	int height = static_cast<INT>(dpiY * 600.f / 96.f);

	HWND hwnd = CreateWindow(TEXT("InitD2D"), TEXT("Init D2D"), WS_OVERLAPPEDWINDOW, 0, 0, width, height, NULL, NULL, hInstance, NULL);

	
	ShowWindow(hwnd, iCmdShow);

	d2dApp.Init(hwnd);

	UpdateWindow(hwnd);


	MSG msg;

	while (GetMessage(&msg, NULL, 0, 0)){

		TranslateMessage(&msg);

		DispatchMessage(&msg);
	}

	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hInstance, UINT message, WPARAM wparam, LPARAM lparam){


	switch (message)
	{
	case WM_CREATE:

		return 0;
		break;
	case WM_PAINT:
		d2dApp.Draw();
		break;
	case WM_MOUSEMOVE:

		return 0;
		break;
	case WM_SIZE:
		d2dApp.Init(hInstance);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		break;
	default:
		break;
	}
	return DefWindowProc(hInstance, message, wparam, lparam);
}