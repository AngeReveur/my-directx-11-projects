#pragma once
#include <d2d1.h>
class d2dapp
{
	ID2D1Factory* d2dFactory;
	ID2D1HwndRenderTarget* d2dRenderTarget;
	ID2D1SolidColorBrush* brush1;
	ID2D1SolidColorBrush* brush2;
public:
	d2dapp();
	~d2dapp();
	bool Init(HWND window);
	void Draw();
private:
	void DrawGrid();
};

