#include "d2dapp.h"
#include <DirectXMath.h>

d2dapp::d2dapp()
{
	d2dFactory = NULL;
	d2dRenderTarget = NULL;
}


d2dapp::~d2dapp()
{
	d2dFactory->Release();
	d2dRenderTarget->Release();
	brush1->Release();
	brush2->Release();
}

bool d2dapp::Init(HWND window){

	if(d2dFactory) d2dFactory->Release();
	if(d2dRenderTarget) d2dRenderTarget->Release();

	HRESULT hr=D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &d2dFactory);

	if (hr != S_OK){
		return false;
	}

	RECT rc;

	GetClientRect(window, &rc);
	D2D1_RENDER_TARGET_PROPERTIES d2dRenderTargetProperties = D2D1::RenderTargetProperties();

	D2D1_HWND_RENDER_TARGET_PROPERTIES d2dHwnRenderTargetProperties =
		D2D1::HwndRenderTargetProperties(window,
		D2D1::SizeU(rc.right - rc.left,
		rc.bottom - rc.top));
	
	hr = d2dFactory->CreateHwndRenderTarget(d2dRenderTargetProperties, d2dHwnRenderTargetProperties, &d2dRenderTarget);

	if (hr != S_OK){
		return false;
	}

	D2D1::ColorF brushColor1 = D2D1::ColorF(218.0f / 255, 218.0f / 255, 218.0f / 255);
	D2D1::ColorF brushColor2 = D2D1::ColorF(D2D1::ColorF::Crimson);
	d2dRenderTarget->CreateSolidColorBrush(brushColor1, &brush1);
	d2dRenderTarget->CreateSolidColorBrush(brushColor2, &brush2);

	return true;
}

void d2dapp::Draw(){
	d2dRenderTarget->SetAntialiasMode(D2D1_ANTIALIAS_MODE_ALIASED);
	d2dRenderTarget->BeginDraw();
	
	D2D1::ColorF backColor = D2D1::ColorF(255.0f / 255, 255.0f / 255, 255.0f / 255);
	
	

	d2dRenderTarget->Clear(backColor);
	DrawGrid();

	d2dRenderTarget->EndDraw();


}

void d2dapp::DrawGrid(){

	D2D_POINT_2F point1;
	point1.x = 0;
	point1.y = 0;
	D2D_POINT_2F point2;
	int distanceY = 10;
	int distanceX = 10;
	while (point1.y < 1080){

		point1.x = 0;
		point1.y += distanceY;


		point2.x = 1920;
		point2.y = point1.y;

		d2dRenderTarget->DrawLine(point1, point2, brush1, 2.0f);
	}

	while (point1.x < 1920){

		point1.x += distanceX;
		point1.y = 0;


		point2.x = point1.x;
		point2.y = 1080;

		d2dRenderTarget->DrawLine(point1, point2, brush1, 2.0f);
	}
}
