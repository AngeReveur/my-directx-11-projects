// VectorOperations1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <DirectXMath.h>
#include <chrono>
using namespace std;
using namespace std::chrono;
using namespace DirectX;
// Overload the "<<" operators so that we can use cout to
// output XMVECTOR objects.
ostream& operator<<(ostream& os, FXMVECTOR v)
{
	XMFLOAT3 dest;
	XMStoreFloat3(&dest, v);
	os << "(" << dest.x << ", " << dest.y << ", " << dest.z << ")";
	return os;
}

ostream& operator<<(ostream& os, XMFLOAT3 v)
{	
	os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
	return os;
}

int _tmain(int argc, _TCHAR* argv[])
{
	XMVECTOR v1=XMVectorZero();
	XMFLOAT3 v2 = XMFLOAT3(0.0001, 0.00001, 0.00001);
	
	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	for (int i = 0; i < 10000000; i++){
		v2.x = v2.x + v2.x;
		v2.y = v2.y + v2.y;
		v2.z = v2.z + v2.z;
		v1 = v1 + v1;
	}



	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	cout <<"duration: "<< duration<<"\n";

	cout << v1;
	cout << '\n';
	cout << v2;
	return 0;
}

