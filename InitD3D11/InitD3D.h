#include <Windows.h>
#include <assert.h>
#include <d3dx11.h>
#pragma once
class d3dApp
{

public:
	int width;
	int heigth;
private:
	ID3D11Device* d3dDevice;
	D3D_FEATURE_LEVEL featureLevel;
	ID3D11DeviceContext* d3dDeviceContext;
	HWND mainWindow;

	//for creating Swap Chain
	
	IDXGISwapChain* swapChain = NULL;

	//for creating the Render Target View
	ID3D11RenderTargetView* renderTargetView;
	
	//for creating the Depth/Stencil Buffer and View
	ID3D11DepthStencilView* depthStencilView;
	
public:
	d3dApp();
	~d3dApp();
	bool InitD3d(HWND window);
	void RenderFrame();
private:	
	bool CreateDevice();
	UINT checkMultiSampleQualityLevel();
	void CreateSwapChain();
	void CreateRenderTargetView();
	void CreateDepthStencilBufferAndView();
	void SetViewPort();
	
	
};

