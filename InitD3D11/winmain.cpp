#include <Windows.h>
#include "InitD3D.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow);
LRESULT CALLBACK WndProc(HWND hInstance, UINT message, WPARAM wparam, LPARAM lparam);
d3dApp initD3D;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow){


	WNDCLASS wc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hInstance = hInstance;
	wc.lpfnWndProc = WndProc;
	wc.lpszClassName = TEXT("InitD3D");

	wc.lpszMenuName = 0;
	wc.style = CS_HREDRAW | CS_VREDRAW;

	RegisterClass(&wc);

	HWND hwnd = CreateWindow(TEXT("InitD3D"), TEXT("Init D3D"), WS_OVERLAPPEDWINDOW, 100, 100, initD3D.width, initD3D.heigth, NULL, NULL, hInstance, NULL);

	ShowWindow(hwnd, iCmdShow);

	initD3D.InitD3d(hwnd);
	initD3D.RenderFrame();
	UpdateWindow(hwnd);


	MSG msg;

	while (GetMessage(&msg, NULL, 0, 0)){

		TranslateMessage(&msg);

		DispatchMessage(&msg);
	}

	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hInstance, UINT message, WPARAM wparam, LPARAM lparam){


	switch (message)
	{
	case WM_CREATE:

		return 0;
		break;
	case WM_MOUSEMOVE:
		
		return 0;
		break;
	case WM_SIZE:
		initD3D.RenderFrame();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		break;
	default:
		break;
	}
	return DefWindowProc(hInstance, message, wparam, lparam);
}