
#include "InitD3D.h"
#include <D3DX10math.h>


d3dApp::d3dApp() :width(800), heigth(600)
{
}


d3dApp::~d3dApp()
{
}

bool d3dApp::InitD3d(HWND window){

	mainWindow = window;

	

	boolean deviceCreatedOK = false;

	deviceCreatedOK = CreateDevice();

	if (!deviceCreatedOK){
		return false;
	}

	

	CreateSwapChain();
	CreateRenderTargetView();
	CreateDepthStencilBufferAndView();
	SetViewPort();
	

	return true;
}

bool d3dApp::CreateDevice(){

	HRESULT hr = D3D11CreateDevice(NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		D3D11_CREATE_DEVICE_SINGLETHREADED,
		NULL, // pFeatureLevels
		0,
		D3D11_SDK_VERSION,
		&d3dDevice,
		&featureLevel,
		&d3dDeviceContext);

	if (FAILED(hr)){
		MessageBox(0, TEXT("D3D11CreateDevice Failed."), 0, 0);
		return false;
	}

	if (featureLevel != D3D_FEATURE_LEVEL_11_0){
		MessageBox(0, TEXT("Direct3D Feature Level 11 not supported."), 0, 0);
		return false;
	}

}

void d3dApp::CreateSwapChain(){

	IDXGIDevice* dxgiDevice = NULL;
	IDXGIAdapter* dxgiAdapter = NULL;
	IDXGIFactory* dxgiFactory = NULL;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;

	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.Height = heigth;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Width = width;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.Flags = 0;
	swapChainDesc.OutputWindow = mainWindow;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Windowed = TRUE;


	d3dDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevice);
	dxgiDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapter);
	dxgiAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactory);

	dxgiFactory->CreateSwapChain(d3dDevice, &swapChainDesc, &swapChain);

	dxgiFactory->MakeWindowAssociation(mainWindow, DXGI_MWA_NO_ALT_ENTER);

	dxgiFactory->Release();
	dxgiAdapter->Release();
	dxgiDevice->Release();
}

void d3dApp::CreateRenderTargetView(){
	
	ID3D11Texture2D* backBuffer;

	swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),
		reinterpret_cast<void**>(&backBuffer));

	d3dDevice->CreateRenderTargetView(backBuffer, 0, &renderTargetView);

	backBuffer->Release();

}

void d3dApp::CreateDepthStencilBufferAndView(){
	
	D3D11_TEXTURE2D_DESC depthStencilDesc;
	ID3D11Texture2D* depthStencilBuffer;
	

	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.Height = heigth;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.MiscFlags = 0;
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;

	d3dDevice->CreateTexture2D(&depthStencilDesc, 0, &depthStencilBuffer);

	d3dDevice->CreateDepthStencilView(depthStencilBuffer, 0, &depthStencilView);

	d3dDeviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

}

void d3dApp::SetViewPort(){

	D3D11_VIEWPORT viewPort;

	viewPort.Height = heigth;
	viewPort.MaxDepth = 1;
	viewPort.MinDepth = 0;
	viewPort.TopLeftX = 0;
	viewPort.TopLeftY = 0;
	viewPort.Width =width;

	d3dDeviceContext->RSSetViewports(1, &viewPort);
}

void d3dApp::RenderFrame(){
	if (d3dDevice == NULL){
		return;
	}

	D3DXCOLOR steelBlueColor = D3DXCOLOR(0x4682B4);
	//float color[4] = { 1.0f, 0.0f, 0.0f, 0.0f };

	d3dDeviceContext->ClearRenderTargetView(renderTargetView, steelBlueColor);

	d3dDeviceContext->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	swapChain->Present(0, 0);
}

UINT d3dApp::checkMultiSampleQualityLevel(){

	UINT m4xMsaaQuality;
	d3dDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m4xMsaaQuality);

	return m4xMsaaQuality;
}